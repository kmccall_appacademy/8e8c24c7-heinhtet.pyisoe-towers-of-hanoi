class TowersOfHanoi
attr_reader :towers

  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def play

    until won?
    render
    puts "Choose where from and where to:"
    move_choice = gets.chomp
    move_choice = move_choice.split(" ")
    move_choice.collect! {|el| el.to_i}
      if valid_move?(move_choice[0], move_choice[1])
      move(move_choice[0], move_choice[1])
      #repeat loop
      else
      puts "Please pick a valid move"
      end
    end

  end

  def render
    puts "TOWER ZERO   TOWER 1   TOWER 2"
    puts "#{towers[0]}     #{towers[1]}     #{towers[2]} "
  end

  def move(from_tower, to_tower)
    towers[to_tower].push(towers[from_tower].pop)
  end

  def valid_move?(from_tower, to_tower)
    return false if from_tower < 0 || from_tower > 3
    return false if towers[from_tower].empty?
    return false if !towers[to_tower].empty? &&
                    towers[from_tower].last > towers[to_tower].last
    true
  end

  def won?
     return true if towers[1].length == 3 || towers[2].length == 3
     false
  end

end
